#!/usr/bin/env python
# -*- coding: utf-8 -*-


# importation des modules utiles :
# import of useful modules:
from __future__ import division, print_function
import turtle

# et d'autres modules qui peuvent servir :
# and other modules that can be used:
import random
import time
import math


# &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
#           TRADUCTION DES FONCTIONS TURTLE EN FRANÇAIS
#           TRANSLATION TURTLE FUNCTIONS IN FRENCH
# &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&


# liste des mots-clés pour la coloration syntaxique :
# list of keywords for syntax highlighting:
keywords = (
    'avance', 'av', 'recule', 'rc', 
    'tourneAGauche', 'tg', 'tourneADroite', 'td', 
    'epaisseur', 
    'couleur', 
    'cercle', 
    'ecris', 
    'leveLeCrayon', 'leve', 'baisseLeCrayon', 'baisse', 
    'vaEn', 'va', 'vaX', 'vaY', 'position', 'dimensions', 
    'efface', 'nouveau', 
    'attendre', 
    'racine', 'pi', 'cos', 'sin', 'tan', 'acos', 'asin', 'atan', 
    'nombreAleatoire', 'choisirDans', 
    'debutDeRemplissage', 'remplir', 'finDeRemplissage', 'finRemplir', 
    'vitesse', 'point', 'direction'
    )



# avance de la distance donnée :
# forward of the given distance:
def avance(distance):
    turtle.forward(distance)

def av(distance):
    turtle.forward(distance)

# recule de la distance donnée :
# back of the given distance:
def recule(distance):
    turtle.backward(distance)

def rc(distance):
    turtle.backward(distance)



# tourne à gauche en degrés :
# turns left in degrees:
def tourneAGauche(angle):
    turtle.left(angle)

def tg(angle):
    turtle.left(angle)

# tourne à droite en degrés :
# turn right in degrees:
def tourneADroite(angle):
    turtle.right(angle)

def td(angle):
    turtle.right(angle)


# direction en degrés
def direction(cap=None):
    if cap is not None:
        turtle.setheading(cap)

    return turtle.heading()


# épaisseur du trait :
# line thickness:
def epaisseur(valeur):
    turtle.width(valeur)



# couleur du crayon :
# pen color:
def couleur(name='', rouge=-1, vert=-1, bleu=-1):
    translations = {
        'rouge':'red', 'vert':'green', 'bleu':'blue', 'noir':'black', 'blanc':'white', 
        'jaune':'yellow', 'orange':'orange', 
        'violet':'violet',}
    if name == '' and rouge == -1 and vert == -1 and bleu == -1:
        r = hex(random.randint(0, 15))
        g = hex(random.randint(0, 15))
        b = hex(random.randint(0, 15))
        color = '#{0}{1}{2}'.format(r[2:], g[2:], b[2:])
    else:
        if rouge == -1:
            color = translations.get(name, 'black')
        else:
            if bleu == -1:
                # shift left the argument
                (rouge, vert, bleu) = (name, rouge, vert)
            color = (rouge, vert, bleu)
    turtle.color(color)



# dessine un cercle de rayon donné :
# draws a circle with the given radius:
def cercle(rayon, angle=None, pas=None):
    turtle.circle(rayon, angle, pas)



# écrit un texte :
# written text:
def ecris(texte, taille=8):
    turtle.write(texte, font=('Arial', taille, 'normal'))



# dessine un point
# draws a dot
def point(rayon=None):
    turtle.dot(rayon)



# relève le crayon :
# lifts the pen:
def leveLeCrayon():
    turtle.up()

def leve():
    turtle.up()

# baisse le crayon :
# lower the pencil:
def baisseLeCrayon():
    turtle.down()

def baisse():
    turtle.down()



# déplace la tortue à la position (x, y) :
# moves the turtle position (x, y):
def vaEn(x, y, trace=False):
    down = turtle.isdown()
    if not(trace):
        turtle.up()
    turtle.goto(x, y)
    if not(trace) and down:
        turtle.down()

def va(x, y, trace=False):
    vaEn(x, y, trace)

# déplace la tortue horizontalement :
# moves the turtle horizontally:
def vaX(x, trace=False):
    down = turtle.isdown()
    if not(trace):
        turtle.up()
    turtle.setx(x)
    if not(trace) and down:
        turtle.down()

# déplace la tortue verticalement :
# moves the turtle vertically:
def vaY(y, trace=False):
    down = turtle.isdown()
    if not(trace):
        turtle.up()
    turtle.sety(y)
    if not(trace) and down:
        turtle.down()

# retourne les coordonnées de la tortue :
# returns the coordinates of the turtle:
def position():
    return turtle.position()

# retourne les dimensions de la fenêtre :
# returns the window size:
def dimensions():
    return (turtle.window_width(), turtle.window_height())


# efface le dessin :
# clears the drawing:
def efface():
    turtle.clear()

# efface le dessin et remet la tortue au centre :
# erases the drawing and puts the turtle center:
def nouveau():
    turtle.reset()



# fait une pause (durée en secondes) :
# a break (time in seconds):
def attendre(delai=0):
    if delai == 0:
        return
    else:
        time.sleep(delai)



# retourne la racine carrée de x :
# returns the square root of x:
def racine(x):
    return math.sqrt(x)

# retourne une valeur approchée de pi :
# returns an approximate value of pi:
pi = math.pi

# fonctions trigonométriques (angles en degrés) :
# trigonometric functions (angles in degrees):
def cos(a):
    return math.cos(math.radians(a))

def sin(a):
    return math.sin(math.radians(a))

def tan(a):
    return math.tan(math.radians(a))

def acos(x):
    return math.degrees(math.acos(x))

def asin(x):
    return math.degrees(math.asin(x))

def atan(x):
    return math.degrees(math.atan(x))







# retourne un entier au hasard inférieur ou égal au nombre indiqué :
# returns an integer less than or equal to the random number indicated:
def nombreAleatoire(n):
    return random.randint(1, int(n))

# retourne une valeur prise au hasard dans une liste :
# returns a value taken at random from a list:
def choisirDans(liste):
    return random.choice(liste)



def debutDeRemplissage():
    turtle.begin_fill()

def remplir():
    turtle.begin_fill()

def finDeRemplissage():
    turtle.end_fill()

def finRemplir():
    turtle.end_fill()


def vitesse(acceleration):
    accelerations = {
        "rapide": "fastest",
        "normale": "normal",
        "lente": "slowest"
        }
    turtle.speed(accelerations[acceleration])
