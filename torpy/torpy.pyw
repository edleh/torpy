#!/usr/bin/env python
# -*- coding: utf-8 -*-

# -----------------------------------------------------------------
# This file is a part of TorPy project.
# Name:         TorPy: vernacular Python Turtle
# Copyright:    (C) 2014-2017 TorPy authors
# Licence:      GNU General Public Licence version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
# -----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------
# Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le
# modifier conformément aux dispositions de la Licence Publique Générale GNU,
# telle que publiée par la Free Software Foundation ; version 3 de la licence,
# ou encore toute version ultérieure.
#
# Ce programme est distribué dans l'espoir qu'il sera utile,
# mais SANS AUCUNE GARANTIE ; sans même la garantie implicite de
# COMMERCIALISATION ou D'ADAPTATION A UN OBJET PARTICULIER. Pour plus de
# détail, voir la Licence Publique Générale GNU.
#
# Vous devez avoir reçu un exemplaire de la Licence Publique Générale
# GNU en même temps que ce programme ; si ce n'est pas le cas, voir
# <http://www.gnu.org/licenses/>.
# -------------------------------------------------------------------------------


"""
DESCRIPTION :
    Fichier de lancement du logiciel.
"""


# importation des modules utiles :
import sys
import os

# récupération du chemin :
HERE = os.path.dirname(os.path.realpath(__file__))
# ajout du chemin au path (+ libs) :
sys.path.insert(0, HERE)
sys.path.insert(0, HERE + os.sep + 'libs')
# on démarre dans le bon dossier :
os.chdir(HERE)

# importation des modules perso :
import utils
import main

# PyQt5, PyQt4 ou PySide :
if utils.PYQT == 'PYQT5':
    from PyQt5 import QtCore, QtWidgets, QtGui
elif utils.PYQT == 'PYQT4':
    from PyQt4 import QtCore, QtGui as QtWidgets, QtGui
else:
    from PySide import QtCore, QtGui as QtWidgets, QtGui



if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)

    pixmap = QtGui.QPixmap('./images/splash.png')
    splash = QtWidgets.QSplashScreen(pixmap, QtCore.Qt.Dialog)
    splash.setMask(pixmap.mask())
    splash.show()
    app.processEvents()

    #******************************************
    # Installation de l'internationalisation :
    #******************************************
    locale = QtCore.QLocale.system().name()
    # recherche d'un i18n passé en argument (par exemple LANG=fr_FR) :
    for arg in sys.argv:
        if arg.split('=')[0] == 'LANG':
            locale = arg.split('=')[1]
    # traduction de Qt (boutons des dialogues, etc) :
    qtTranslationsPath = QtCore.QLibraryInfo.location(
        QtCore.QLibraryInfo.TranslationsPath)
    qtTranslator = QtCore.QTranslator()
    if qtTranslator.load('qt_' + locale, qtTranslationsPath):
        app.installTranslator(qtTranslator)
    # traduction du logiciel :
    appTranslationsPath = QtCore.QDir('./translations').canonicalPath()
    appLocalefile = '{0}_{1}'.format(utils.PROGNAME, locale)
    appTranslator = QtCore.QTranslator()
    if appTranslator.load(appLocalefile, appTranslationsPath):
        app.installTranslator(appTranslator)

    #******************************************
    # Lancement du logiciel :
    #******************************************
    app.setWindowIcon(QtGui.QIcon('./images/torpy.png'))

    mainWindow = main.MainWindow(locale, appTranslator, splash)
    mainWindow.show()

    if len(sys.argv) > 1:
        mainWindow.openFile(sys.argv[-1])

    sys.exit(app.exec_())


