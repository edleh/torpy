# TorPy

#### vernacular Python Turtle

* **Website:** http://pascal.peter.free.fr/wiki/Programmation/Torpy
* **Email:** pascal.peter at free.fr
* **License:** GNU General Public License (version 3)
* **Copyright:** (c) 2014-2017

----

## The tools used to develop TorPy

#### Basic Tools:
* [Python](http://www.python.org): programming language
* [Qt](http://qt-project.org): "toolkit" comprehensive (graphical user interface and a lot of things)
* [PyQt](http://www.riverbankcomputing.co.uk/software/pyqt/intro): link between Python and Qt

#### Other libraries used and other stuff:
* [Blender](http://www.blender.org): to create the logo
* [Python syntax highlighting](https://wiki.python.org/moin/PyQt/Python syntax highlighting): for syntax highlighting
* [pstoedit](http://www.pstoedit.net): translates PostScript graphics into others formats
* [marked](https://github.com/chjj/marked): to view the Markdown files

#### Miscellaneous:
* [GNU GPL 3](http://www.gnu.org/copyleft/gpl.html): GNU General Public License
