# TorPy

#### la Tortue Python vernaculaire

* **Site Web :** http://pascal.peter.free.fr/wiki/Programmation/Torpy
* **Email :** pascal.peter at free.fr
* **Licence :** GNU General Public License (version 3)
* **Copyright :** (c) 2014-2017

----

## Les outils utilisés pour développer TorPy

#### Outils de base :
* [Python](http://www.python.org) : langage de programmation
* [Qt](http://qt-project.org) : "toolkit" très complet (interface graphique et tout un tas de choses)
* [PyQt](http://www.riverbankcomputing.co.uk/software/pyqt/intro) : lien entre Python et Qt

#### Autres bibliothèques utilisées et trucs divers :
* [Blender](http://www.blender.org) : pour la création du logo
* [Python syntax highlighting](https://wiki.python.org/moin/PyQt/Python syntax highlighting) : pour la coloration syntaxique
* [pstoedit](http://www.pstoedit.net) : transforme les graphiques PostScript dans d'autres formats
* [marked](https://github.com/chjj/marked) : pour afficher les fichiers Markdown

#### Divers :
* [GNU GPL 3](http://www.gnu.org/copyleft/gpl.html) : licence publique générale GNU
