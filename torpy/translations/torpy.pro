SOURCES      += ../torpy.pyw

SOURCES      += ../libs/main.py
SOURCES      += ../libs/utils.py
SOURCES      += ../libs/utils_about.py
SOURCES      += ../libs/utils_filesdirs.py
SOURCES      += ../libs/utils_functions.py
SOURCES      += ../libs/utils_python.py

TRANSLATIONS += ../translations/torpy.ts
TRANSLATIONS += ../translations/torpy_fr.ts
