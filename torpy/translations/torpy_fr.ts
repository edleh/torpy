<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr">
<context>
    <name>main</name>
    <message>
        <location filename="../libs/main.py" line="496"/>
        <source>Base Bar</source>
        <translation>Barre principale</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="504"/>
        <source>E&amp;xit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="504"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="607"/>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="560"/>
        <source>F1</source>
        <translation>F1</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="576"/>
        <source>&amp;About</source>
        <translation>À &amp;propos</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="586"/>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="104"/>
        <source>About</source>
        <translation>À propos</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="111"/>
        <source>Credits</source>
        <translation type="obsolete">Remerciements</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="107"/>
        <source>License</source>
        <translation>Licence</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="115"/>
        <source>&amp;Close</source>
        <translation>&amp;Fermer</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="127"/>
        <source>About {0}</source>
        <translation>À propos de {0}</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="170"/>
        <source>information message</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="171"/>
        <source>question message</source>
        <translation>Question</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="172"/>
        <source>warning message</source>
        <translation>Attention</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="173"/>
        <source>critical message</source>
        <translation>Message critique</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="90"/>
        <source>About {0} (version {1})</source>
        <translation>À propos de {0} (version {1})</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="310"/>
        <source>Choose the Directory where the desktop file will be created</source>
        <translation>Choisissez le dossier où le lanceur sera créé</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="553"/>
        <source>Create a launcher</source>
        <translation>Créer un lanceur</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="553"/>
        <source>To create a launcher file (*.desktop) in the folder of your choice</source>
        <translation>Pour créer un lanceur (fichier *.desktop) dans le dossier de votre choix</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="187"/>
        <source>Open a File</source>
        <translation>Ouvrir un fichier</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="270"/>
        <source>No name</source>
        <translation>Sans nom</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="292"/>
        <source>The file has been modified.
Do you want to save your changes?</source>
        <translation>Le fichier a été modifié.
Voulez-vous enregistrer les changements ?</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="510"/>
        <source>&amp;New</source>
        <translation>&amp;Nouveau</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="510"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="510"/>
        <source>New file</source>
        <translation>Créer un nouveau fichier</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="518"/>
        <source>&amp;Open</source>
        <translation>&amp;Ouvrir</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="518"/>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="518"/>
        <source>Open a file</source>
        <translation>Ouvrir un fichier</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="526"/>
        <source>&amp;Save</source>
        <translation>&amp;Enregistrer</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="526"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="526"/>
        <source>Save a file</source>
        <translation>Enregistrer le fichier</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="534"/>
        <source>Save &amp;as</source>
        <translation>Enregistrer &amp;sous</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="534"/>
        <source>Save the file as...</source>
        <translation>Enregistrer le fichier sous...</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="568"/>
        <source>&amp;Project Web Page</source>
        <translation>&amp;Site du projet</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="560"/>
        <source>&amp;Instructions</source>
        <translation>&amp;Instructions</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="560"/>
        <source>Display list of available instructions</source>
        <translation>Afficher la liste des instructions disponibles</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="541"/>
        <source>&amp;Run the Turtle</source>
        <translation>&amp;Démarrer la tortue</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="541"/>
        <source>Execute instructions by the Turtle</source>
        <translation>Faire exécuter les instructions par la tortue</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="602"/>
        <source>&amp;Run</source>
        <translation>&amp;Exécuter</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="220"/>
        <source>Save a File</source>
        <translation>Enregistrer un fichier</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="547"/>
        <source>Save &amp;images</source>
        <translation>Enregistrer en &amp;images</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="547"/>
        <source>Save the Turtle window as images files</source>
        <translation>Enregistrer la fenêtre de la Tortue en fichiers images</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="568"/>
        <source>View project website in your browser</source>
        <translation>Afficher le site web du projet dans votre navigateur</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="215"/>
        <source>END !</source>
        <translation>TERMINÉ !</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="216"/>
        <source>Images are saved in the folder:</source>
        <translation>Les images sont enregistrées dans le dossier :</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="73"/>
        <source>Text files</source>
        <translation>Fichiers texte</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="74"/>
        <source>All files</source>
        <translation>Tous les fichiers</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="590"/>
        <source>&amp;Recent Files</source>
        <translation>Fichiers &amp;récents</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="110"/>
        <source>Authors</source>
        <translation>Auteurs</translation>
    </message>
</context>
</TS>
